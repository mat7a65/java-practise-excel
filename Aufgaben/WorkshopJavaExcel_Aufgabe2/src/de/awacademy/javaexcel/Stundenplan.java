package de.awacademy.javaexcel;

import java.util.ArrayList;
import java.util.List;

public class Stundenplan {
    private List<Unterrichtsfach> unterrichtsfaecher;


    public Stundenplan() {
        this.unterrichtsfaecher = new ArrayList<>();
    }

    public void addUnterrichtsfach(Unterrichtsfach unterrichtsfach) {
        unterrichtsfaecher.add(unterrichtsfach);
    }

    public List<Unterrichtsfach> getUnterrichtsfaecher() {
        return unterrichtsfaecher;
    }
}
