package de.awacademy.javaexcel;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class A06_StundenplanWriter_Aufgabe2 {

    private List<Lehrkraft> lehrkraefte;

    private List<Fach> faecher;

    private List<Schulklasse> schulklassen;


    public static void main(String[] args) throws IOException {
        new A06_StundenplanWriter_Aufgabe2().run();
    }

    private void run() throws IOException {
        Workbook workbook = WorkbookFactory.create(new File("schule.xlsx"), null, true);

        this.faecher = readFaecher(workbook);
        this.lehrkraefte = readLehrkraefte(workbook);
        this.schulklassen = readKlassen(workbook);

        Stundenplan stundenplan = createStundenplan();

        // Aufgabe 2: Erzeuge aus dem Stundenplan eine Excel-Datei, die das Format der Datei stundenplan_struktur.xlsx hat
        //            Für jede Klasse und für jede Lehrkraft soll ein eigenes Arbeitsblatt erstellt werden.
        //            Für jede Klasse ist aufgelistet, welches Fach sie bei welchem Lehrer hat
        //            Für jede Lehrkraft ist aufgelistet, welches Fach sie in welcher Klasse unterrichtet
    }

    private Stundenplan createStundenplan() {
        Stundenplan plan = new Stundenplan();

        int lehrkraftIndex = 0;
        for (Schulklasse schulklasse : schulklassen) {
            for (Fach fach : faecher) {
                if (fach.getStundenzahlJeKlassenstufe().get(schulklasse.getKlassenstufe() - 1) == 0) {
                    continue;
                }
                Lehrkraft lehrkraft;
                do {
                    lehrkraft = lehrkraefte.get(lehrkraftIndex++ % lehrkraefte.size());
                } while (!lehrkraft.getFaecher().contains(fach));
                Unterrichtsfach unterrichtsfach = new Unterrichtsfach(schulklasse, fach, lehrkraft);
                plan.addUnterrichtsfach(unterrichtsfach);
                System.out.println(unterrichtsfach);
            }
        }

        return plan;
    }

    private List<Fach> readFaecher(Workbook workbook) {
        List<Fach> faecher = new ArrayList<>();
        Sheet sheet = workbook.getSheet("Fächer");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // skip headline
                continue;
            }
            String name = row.getCell(0).getStringCellValue();
            String abbreviation = row.getCell(1).getStringCellValue();
            List<Integer> sollStundenzahlen = new ArrayList<>();
            for (int klassenstufe = 2; klassenstufe <= 5; klassenstufe++) {
                int sollStundenzahl = (int) row.getCell(klassenstufe).getNumericCellValue();
                sollStundenzahlen.add(sollStundenzahl);
            }
            Fach fach = new Fach(name, abbreviation, sollStundenzahlen);
            faecher.add(fach);
            System.out.println(fach);
        }
        return faecher;
    }

    private List<Lehrkraft> readLehrkraefte(Workbook workbook) {
        List<Lehrkraft> lehrkraefte = new ArrayList<>();
        Sheet sheet = workbook.getSheet("Lehrkräfte");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // skip headline
                continue;
            }
            String name = row.getCell(0).getStringCellValue();
            int sollStundenzahl = (int) row.getCell(6).getNumericCellValue();
            List<Fach> faecherListe = new ArrayList<>();
            for (int fachNr = 1; fachNr <= 5; fachNr++) {
                Cell cell = row.getCell(fachNr);
                if (cell != null) {
                    String fachKuerzel = cell.getStringCellValue();
                    faecherListe.add(getFachByAbbreviation(fachKuerzel));
                }
            }
            Lehrkraft lehrkraft = new Lehrkraft(name, faecherListe, sollStundenzahl);
            lehrkraefte.add(lehrkraft);
            System.out.println(lehrkraft);
        }
        return lehrkraefte;
    }

    private List<Schulklasse> readKlassen(Workbook workbook) {
        List<Schulklasse> schulklassen = new ArrayList<>();
        Sheet sheet = workbook.getSheet("Klassen");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // skip headline
                continue;
            }
            String kuerzel = row.getCell(0).getStringCellValue();
            String name = row.getCell(1).getStringCellValue();
            Schulklasse schulklasse = new Schulklasse(kuerzel, name);
            schulklassen.add(schulklasse);
            System.out.println(schulklasse);
        }
        return schulklassen;
    }

    private Fach getFachByAbbreviation(String fachKuerzel) {
        for (Fach fach : faecher) {
            if (fach.getAbkuerzung().equals(fachKuerzel)) {
                return fach;
            }
        }
        return null;
    }
}
