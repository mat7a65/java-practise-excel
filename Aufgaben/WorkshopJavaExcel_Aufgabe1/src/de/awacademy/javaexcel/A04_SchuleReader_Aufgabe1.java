package de.awacademy.javaexcel;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class A04_SchuleReader_Aufgabe1 {

    private List<Lehrkraft> lehrkraefte;

    private List<Fach> faecher;

    private List<Schulklasse> schulklassen;


    public static void main(String[] args) throws IOException {
        new A04_SchuleReader_Aufgabe1().run();
    }

    private void run() throws IOException {
        Workbook workbook = WorkbookFactory.create(new File("schule.xlsx"), null, true);

        // Ausgabe 1: Lies die Arbeitsblätter aus der Excel-Datei ein und
        //            gib jeden Datensatz mit dessen toString()-Methode auf der Konsole aus.
    }
}
