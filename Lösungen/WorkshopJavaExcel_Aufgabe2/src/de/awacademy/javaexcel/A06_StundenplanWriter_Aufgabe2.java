package de.awacademy.javaexcel;

import org.apache.commons.compress.parallel.ScatterGatherBackingStore;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class A06_StundenplanWriter_Aufgabe2 {

    private List<Lehrkraft> lehrkraefte;

    private List<Fach> faecher;

    private List<Schulklasse> schulklassen;

    public static void main(String[] args) throws IOException {
        new A06_StundenplanWriter_Aufgabe2().run();
    }

    public static void makeCellBold(Workbook workbook, Cell cell) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        cell.setCellStyle(style);
    }

    private void run() throws IOException {
        Workbook workbook = WorkbookFactory.create(new File("schule.xlsx"), null, true);
        Workbook workbookStundenplan = new XSSFWorkbook();

        this.faecher = readFaecher(workbook);
        this.lehrkraefte = readLehrkraefte(workbook);
        this.schulklassen = readKlassen(workbook);

        Stundenplan stundenplan = createStundenplan();

        savePlan(stundenplan, workbookStundenplan);

        // Aufgabe 2: Erzeuge aus dem Stundenplan eine Excel-Datei, die das Format der Datei stundenplan_struktur.xlsx hat
        //            Für jede Klasse und für jede Lehrkraft soll ein eigenes Arbeitsblatt erstellt werden.
        //            Für jede Klasse ist aufgelistet, welches Fach sie bei welchem Lehrer hat
        //            Für jede Lehrkraft ist aufgelistet, welches Fach sie in welcher Klasse unterrichtet
    }

//    Solution from mat7a65

    private Stundenplan createStundenplan() {
        Stundenplan plan = new Stundenplan();

        int lehrkraftIndex = 0;
        for (Schulklasse schulklasse : schulklassen) {
            for (Fach fach : faecher) {
                if (fach.getStundenzahlJeKlassenstufe().get(schulklasse.getKlassenstufe() - 1) == 0) {
                    continue;
                }
                Lehrkraft lehrkraft;
                do {
                    lehrkraft = lehrkraefte.get(lehrkraftIndex++ % lehrkraefte.size());
                } while (!lehrkraft.getFaecher().contains(fach));
                Unterrichtsfach unterrichtsfach = new Unterrichtsfach(schulklasse, fach, lehrkraft);
                plan.addUnterrichtsfach(unterrichtsfach);
                System.out.println(unterrichtsfach);
            }
        }

        return plan;
    }

    private void savePlan(Stundenplan stundenplan, Workbook workbookStundenplan) throws IOException {

        Map<String, Schulklasse> schulklassenMap = new HashMap<>();
        Map<String, String> tempMap = new HashMap<>();
        Map<String, List<Unterrichtsfach>> mapKlasseFach = new HashMap<>();

//        Schulklassen

        stundenplan.getUnterrichtsfaecher().stream()
                .forEach(unterrichtsfach -> {
                    schulklassenMap.put(unterrichtsfach.getSchulklasse().getName(), unterrichtsfach.getSchulklasse());
                });

        schulklassenMap.forEach((a, b) -> {
            tempMap.put(b.getKuerzel(), a);
        });

        Map<String, String> sorted = new TreeMap<>(tempMap);
        sorted.forEach((a, b) -> {
            workbookStundenplan.createSheet(a + " " + b);
        });

        schulklassenMap.forEach((klassenName, schulklasse) -> {
            List<Unterrichtsfach> unterrichtsfachList = new ArrayList<>();
            stundenplan.getUnterrichtsfaecher().forEach(unterrichtsfach -> {
                if (klassenName.equals(unterrichtsfach.getSchulklasse().getName())) {
                    unterrichtsfachList.add(unterrichtsfach);
                }
            });
            mapKlasseFach.put(schulklasse.getKuerzel() + " " + klassenName, unterrichtsfachList);
        });


        mapKlasseFach.forEach((klasse, unterrichtsfachList) -> {
            Sheet currentSheetKlasse = workbookStundenplan.getSheet(klasse);

            if (currentSheetKlasse.getRow(0) == null) {
                currentSheetKlasse.createRow(0);
            }
            if (currentSheetKlasse.getRow(0).getCell(0) == null) {
                currentSheetKlasse.getRow(0).createCell(0);
            }
            Cell headlineCellFach = currentSheetKlasse.getRow(0).createCell(0);
            headlineCellFach.setCellValue("Fach");
            makeCellBold(workbookStundenplan, headlineCellFach);

            if (currentSheetKlasse.getRow(0) == null) {
                currentSheetKlasse.createRow(0);
            }
            if (currentSheetKlasse.getRow(0).getCell(1) == null) {
                currentSheetKlasse.getRow(0).createCell(1);
            }

            Cell headlineCellLehrkraft = currentSheetKlasse.getRow(0).createCell(1);
            headlineCellLehrkraft.setCellValue("Lehrkraft");
            makeCellBold(workbookStundenplan, headlineCellLehrkraft);

            List<Fach> fachList = new ArrayList<>();
            unterrichtsfachList.forEach(unterrichtsfach -> {
                fachList.add(unterrichtsfach.getFach());
            });

            List<Lehrkraft> lehrkraftList = new ArrayList<>();
            unterrichtsfachList.forEach(unterrichtsfach -> {
                lehrkraftList.add(unterrichtsfach.getLehrkraft());
            });

            for (int i = 1; i <= unterrichtsfachList.size(); i++) {
                if (currentSheetKlasse.getRow(i) == null) {
                    currentSheetKlasse.createRow(i);
                }
                if (currentSheetKlasse.getRow(i).getCell(0) == null) {
                    currentSheetKlasse.getRow(i).createCell(0);
                }
                currentSheetKlasse.getRow(i).getCell(0).setCellValue(fachList.get(i - 1).getName());
            }

            for (int i = 1; i <= unterrichtsfachList.size(); i++) {
                if (currentSheetKlasse.getRow(i) == null) {
                    currentSheetKlasse.createRow(i);
                }
                if (currentSheetKlasse.getRow(i).getCell(1) == null) {
                    currentSheetKlasse.getRow(i).createCell(1);
                }
                currentSheetKlasse.getRow(i).getCell(1).setCellValue(lehrkraftList.get(i - 1).getName());

                currentSheetKlasse.autoSizeColumn(0);
                currentSheetKlasse.autoSizeColumn(1);
            }

//            Lehrkräfte
            Set<Lehrkraft> lehrkraftSet = new HashSet<>();
            List<Lehrkraft> lehrkraftListSorted = new ArrayList<>();
            Map<Lehrkraft, List<Unterrichtsfach>> lehrkraftUnterrichtsfachMap = new HashMap<>();

            stundenplan.getUnterrichtsfaecher()
                    .forEach(unterrichtsfach -> {
                        lehrkraftSet.add(unterrichtsfach.getLehrkraft());
                    });

            lehrkraftListSorted = lehrkraftSet.stream()
                    .collect(Collectors.toList())
                    .stream()
                    .sorted(Comparator.comparing(Lehrkraft::getName))
                    .collect(Collectors.toList());

            lehrkraftListSorted.forEach(lehrkraft -> {
                if (workbookStundenplan.getSheet(lehrkraft.getName()) == null) {
                    workbookStundenplan.createSheet(lehrkraft.getName());
                }
            });

            lehrkraftListSorted.forEach(lehrkraft -> {
                List<Unterrichtsfach> unterrichtsfachListLeherer = new ArrayList<>();

                stundenplan.getUnterrichtsfaecher().forEach(unterrichtsfach -> {
                    if (lehrkraft.getName().equals(unterrichtsfach.getLehrkraft().getName())) {
                        unterrichtsfachListLeherer.add(unterrichtsfach);
                    }
                });
                lehrkraftUnterrichtsfachMap.put(lehrkraft, unterrichtsfachListLeherer);
            });

            lehrkraftUnterrichtsfachMap.forEach((lehrkraft, unterrichtsfachListLehrer) -> {

                Map<String, List<Fach>> klassenFachMap = new HashMap<>();
                Set<String> klassenKuerzelSet = new HashSet<>();

                unterrichtsfachListLehrer.forEach(unterrichtsfach -> {
                    String klassenKuerzel = unterrichtsfach.getSchulklasse().getKuerzel();
                    klassenKuerzelSet.add(klassenKuerzel);
                });

                klassenKuerzelSet.forEach(kuerzel -> {
                    List<Fach> fachListe = new ArrayList<>();

                    unterrichtsfachListLehrer.forEach(unterrichtsfach -> {
                        if (unterrichtsfach.getSchulklasse().getKuerzel().equals(kuerzel)) {
                            fachListe.add(unterrichtsfach.getFach());
                        }
                    });
                    klassenFachMap.put(kuerzel, fachListe);
                });

                Map<String, List<Fach>> klassenFachTreeMap = new TreeMap<>(klassenFachMap);

                Sheet currentSheetLehrer = workbookStundenplan.getSheet(lehrkraft.getName());

                if (currentSheetLehrer.getRow(0) == null) {
                    currentSheetLehrer.createRow(0);
                }

                if (currentSheetLehrer.getRow(0).getCell(0) == null) {
                    currentSheetLehrer.getRow(0).createCell(0);
                }

                Cell headlineKlasse = currentSheetLehrer.getRow(0).getCell(0);
                headlineKlasse.setCellValue("Klasse");
                makeCellBold(workbookStundenplan, headlineKlasse);


                if (currentSheetLehrer.getRow(0) == null) {
                    currentSheetLehrer.createRow(0);
                }

                if (currentSheetLehrer.getRow(0).getCell(1) == null) {
                    currentSheetLehrer.getRow(0).createCell(1);
                }

                Cell headlineFaecher = currentSheetLehrer.getRow(0).getCell(1);
                headlineFaecher.setCellValue("Fächer");
                makeCellBold(workbookStundenplan, headlineFaecher);

                List<String> kuerzel = new ArrayList<>();

                klassenFachTreeMap.forEach((keyKuerzel, valueFachListe) -> {
                    kuerzel.add(keyKuerzel);
                });

                for (int i = 1; i <= kuerzel.size(); i++) {
                    if (currentSheetLehrer.getRow(i) == null) {
                        currentSheetLehrer.createRow(i);
                    }
                    if (currentSheetLehrer.getRow(i).getCell(0) == null) {
                        currentSheetLehrer.getRow(i).createCell(0).setCellValue(kuerzel.get(i - 1));
                        currentSheetKlasse.autoSizeColumn(0);
                    }
                    List<Fach> tempList = new ArrayList<>();
                    tempList.addAll(klassenFachTreeMap.get(kuerzel.get(i - 1)));

                    for (int j = 1; j <= tempList.size(); j++) {
                        if (currentSheetLehrer.getRow(i).getCell(j) == null) {
                            currentSheetLehrer.getRow(i).createCell(j);
                        }
                        currentSheetLehrer.getRow(i).getCell(j).setCellValue(tempList.get(j - 1).getName());
                        currentSheetKlasse.autoSizeColumn(j);
                    }
                }


                try (FileOutputStream fos =
                             new FileOutputStream(new File("stundenplan.xlsx"))) {
                    workbookStundenplan.write(fos);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
    }


    private List<Fach> readFaecher(Workbook workbook) {
        List<Fach> faecher = new ArrayList<>();
        Sheet sheet = workbook.getSheet("Fächer");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // skip headline
                continue;
            }
            String name = row.getCell(0).getStringCellValue();
            String abbreviation = row.getCell(1).getStringCellValue();
            List<Integer> sollStundenzahlen = new ArrayList<>();
            for (int klassenstufe = 2; klassenstufe <= 5; klassenstufe++) {
                int sollStundenzahl = (int) row.getCell(klassenstufe).getNumericCellValue();
                sollStundenzahlen.add(sollStundenzahl);
            }
            Fach fach = new Fach(name, abbreviation, sollStundenzahlen);
            faecher.add(fach);
            System.out.println(fach);
        }
        return faecher;
    }

    private List<Lehrkraft> readLehrkraefte(Workbook workbook) {
        List<Lehrkraft> lehrkraefte = new ArrayList<>();
        Sheet sheet = workbook.getSheet("Lehrkräfte");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // skip headline
                continue;
            }
            String name = row.getCell(0).getStringCellValue();
            int sollStundenzahl = (int) row.getCell(6).getNumericCellValue();
            List<Fach> faecherListe = new ArrayList<>();
            for (int fachNr = 1; fachNr <= 5; fachNr++) {
                Cell cell = row.getCell(fachNr);
                if (cell != null) {
                    String fachKuerzel = cell.getStringCellValue();
                    faecherListe.add(getFachByAbbreviation(fachKuerzel));
                }
            }
            Lehrkraft lehrkraft = new Lehrkraft(name, faecherListe, sollStundenzahl);
            lehrkraefte.add(lehrkraft);
            System.out.println(lehrkraft);
        }
        return lehrkraefte;
    }

    private List<Schulklasse> readKlassen(Workbook workbook) {
        List<Schulklasse> schulklassen = new ArrayList<>();
        Sheet sheet = workbook.getSheet("Klassen");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // skip headline
                continue;
            }
            String kuerzel = row.getCell(0).getStringCellValue();
            String name = row.getCell(1).getStringCellValue();
            Schulklasse schulklasse = new Schulklasse(kuerzel, name);
            schulklassen.add(schulklasse);
            System.out.println(schulklasse);
        }
        return schulklassen;
    }

    private Fach getFachByAbbreviation(String fachKuerzel) {
        for (Fach fach : faecher) {
            if (fach.getAbkuerzung().equals(fachKuerzel)) {
                return fach;
            }
        }
        return null;
    }
}
