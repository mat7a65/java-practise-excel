package de.awacademy.javaexcel;

public class Unterrichtsfach {
    private Schulklasse schulklasse;

    private Fach fach;

    private Lehrkraft lehrkraft;


    public Unterrichtsfach(Schulklasse schulklasse, Fach fach, Lehrkraft lehrkraft) {
        this.schulklasse = schulklasse;
        this.fach = fach;
        this.lehrkraft = lehrkraft;
    }

    public Schulklasse getSchulklasse() {
        return schulklasse;
    }

    public Fach getFach() {
        return fach;
    }

    public Lehrkraft getLehrkraft() {
        return lehrkraft;
    }

    @Override
    public String toString() {
        return "Unterrichtsfach{" +
                "schulklasse=" + schulklasse +
                ", fach=" + fach +
                ", lehrkraft=" + lehrkraft +
                '}';
    }
}
