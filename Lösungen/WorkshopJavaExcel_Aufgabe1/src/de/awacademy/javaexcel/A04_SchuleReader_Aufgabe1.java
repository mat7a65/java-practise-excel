package de.awacademy.javaexcel;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class A04_SchuleReader_Aufgabe1 {

    private List<Lehrkraft> lehrkraefte = new ArrayList<>();
    private List<Fach> faecher = new ArrayList<>();
    private List<Schulklasse> schulklassen = new ArrayList<>();
    Map<String, Fach> faecherMap = new HashMap<>();

    public static void main(String[] args) throws IOException {
        new A04_SchuleReader_Aufgabe1().run();
    }

    private void run() throws IOException {
        Workbook workbook = WorkbookFactory.create(new File("schule.xlsx"), null, true);

        // Ausgabe 1: Lies die Arbeitsblätter aus der Excel-Datei ein und
        //            gib jeden Datensatz mit dessen toString()-Methode auf der Konsole aus.

        this.schulklassen = readSchulklassen(workbook);
        this.faecher = readFaecher(workbook);
        this.lehrkraefte = readLehrkraefte(workbook);

        ausgabe(this.lehrkraefte, this.faecher, this.schulklassen);

    }

//    Solution from mat7a65

//        Sheet Schulklassen einlesen:
    private List<Schulklasse> readSchulklassen(Workbook workbook) {

        List<Schulklasse> schulklassenList = new ArrayList<>();
        Sheet sheetKlassen = workbook.getSheetAt(2);

        sheetKlassen.forEach(row -> {
            if (row.getRowNum() != 0) {
                schulklassenList.add(new Schulklasse(row.getCell(0).toString(), row.getCell(1).toString()));
            }
        });
        return schulklassenList;
    }

//        Sheet Fächer einlesen

    private List<Fach> readFaecher(Workbook workbook) {

        List<Fach> faecherList = new ArrayList<>();
        Sheet sheetFaecher = workbook.getSheetAt(1);

        sheetFaecher.forEach(row -> {
            if (row.getRowNum() != 0) {
                List<Integer> stundenList = new ArrayList<>();

                row.forEach(cell -> {
                    if (cell.getColumnIndex() >= 2 && cell.getColumnIndex() <= 5) {
                        double d = Double.parseDouble(cell.toString());
                        int stunde = (int) d;
                        stundenList.add(stunde);
                    }
                });

                Fach fach = new Fach(row.getCell(0).toString(), row.getCell(1).toString(), stundenList);
                faecherList.add(fach);
                faecherMap.put(fach.getAbkuerzung(), fach);

            }
        });
        return faecherList;
    }

    //        Sheet Lehrkräfte einlesen
    private List<Lehrkraft> readLehrkraefte(Workbook workbook) {
        List<Lehrkraft> lehrkraefteList = new ArrayList<>();
        Sheet lehrkraefteSheet = workbook.getSheetAt(0);

        lehrkraefteSheet.forEach(row -> {
            if (row.getRowNum() >= 1 && row.getRowNum() <= 5) {
                List<Fach> faecherListe = new ArrayList<>();
                row.forEach(cell -> {
                    if (cell != null) {
                        String key = cell.toString();
                        if (faecherMap.containsKey(key)) {
                            faecherListe.add(faecherMap.get(key));
                        }
                    }
                });
                double d = Double.parseDouble(row.getCell(6).toString());
                int sollStunden = (int) d;

                Lehrkraft lehrkraft = new Lehrkraft(row.getCell(0).toString(), faecherListe, sollStunden);
                lehrkraefteList.add(lehrkraft);
            }
        });
        return lehrkraefteList;
    }

    private void ausgabe(List<Lehrkraft> lehrkaefteList, List<Fach> faecherList, List<Schulklasse> schulklassenList) {

        lehrkaefteList.forEach(lehrkraft -> {
            System.out.println(lehrkraft.toString());
        });

        faecherList.forEach(fach -> {
            System.out.println(fach.toString());
        });

        schulklassenList.forEach(schulklasse -> {
            System.out.println(schulklasse.toString());
        });
    }
}






